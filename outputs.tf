output "dev-vpc-id" {
  value = aws_vpc.development-vpc.id
}
output "ec2_public_ip" {
  value = module.myapp-webserver.ec2_public_ip
}