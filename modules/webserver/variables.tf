variable "avail_zone" {}
variable "instance_type" {}
variable "public_key_location" {}
variable "private_key_location" {}
variable "env_prefix" {
  description = "deployment environment"
  default     = "dev-env"
}
variable "subnet" {}
variable "vpc_id" {}
variable "my_ip" {}