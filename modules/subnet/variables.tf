variable "env_prefix" {
  description = "deployment environment"
  default     = "dev-env"
}
variable "vpc_id" {
}
variable "cidr_blocks" {}
variable "avail_zone_a" {}
variable "avail_zone_b" {}
variable "default_route_table_id" {}