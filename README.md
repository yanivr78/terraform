**Terraform Configuration for MyApp Infrastructure**

This repository contains Terraform code to set up infrastructure for MyApp on AWS. It includes modules for creating VPC subnets and a web server instance with AWS.

**Overview**

The Terraform configuration creates the following resources:

VPC and Subnets: Sets up a VPC and two subnets using the myapp-subnet module.
Web Server: Deploys an EC2 instance in one of the subnets using the myapp-webserver module. The EC2 instance is configured with security groups allowing SSH access and web traffic.

**Prerequisites**

Before you begin, ensure you have the following:

Terraform installed (version X.X.X or later)
An AWS account
AWS CLI configured with appropriate credentials

**Directory Structure**
main.tf: Root module that integrates the subnet and webserver modules.
variables.tf: Defines variables used in the configuration.
terraform.tfvars: Contains values for the defined variables.
outputs.tf: Defines output variables.
modules/
subnet/: Module to create VPC subnets.
webserver/: Module to create an EC2 web server instance.

**Usage**
Setting Up Your Variables
Edit terraform.tfvars to include your specific configuration values like CIDR blocks, availability zones, and IP addresses.

Example:

env_prefix = "dev"
cidr_blocks = [
  { cidr_block = "10.0.0.0/16", name = "dev-vpc" },
  { cidr_block = "10.0.10.0/24", name = "dev-subnet-A" },
  ...
]
Ensure your AWS credentials are configured properly.

**Initializing Terraform**

Run the following command to initialize Terraform, which will download the necessary providers and modules:

terraform init

**Applying the Configuration**

To create the infrastructure, run:

terraform apply
Review the plan and confirm the changes.

**Destroying the Infrastructure**

To tear down the infrastructure and delete all resources, run:

terraform destroy

**Outputs**

dev-vpc-id: The ID of the created VPC.
ec2_public_ip: The public IP address of the EC2 web server instance.
Contributing
To contribute to this repository, please create a branch or fork, make changes, and submit a pull request.

** recommended .gitignore configuration**

```
### IntelliJ IDEA ###
out/
!**/src/main/**/out/
!**/src/test/**/out/

### Eclipse ###
.apt_generated
.classpath
.factorypath
.project
.settings
.springBeans
.sts4-cache
bin/
!**/src/main/**/bin/
!**/src/test/**/bin/

### NetBeans ###
/nbproject/private/
/nbbuild/
/dist/
/nbdist/
/.nb-gradle/

### VS Code ###
.vscode/

### Mac OS ###
.DS_Store

### Remove the terraform folders and files ###
.terraform/*

# tf state files
*.tfstate
*.tfstate.*

# tf variable files may include sensetive data
*.tfvars
```

**License**
All rights resreved to Yaniv Ron.


